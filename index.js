const getCube = 2 ** 3
console.log (`The cube of 2 is ${getCube}`);

const address =[258, 'Washington Ave', 'NW', 'California', '90081'];
const [street, city, state, country, postalCode] = address;
console.log(`I live at ${street} ${city} ${state}, ${country}, ${postalCode}`);

const animalDetails = ['Balot', 'in-house cat', '7 Kilos', '25 cm'];
const [name, type, weight, height] = animalDetails;
console.log(`${name} is an ${type} and weighs ${weight} with a height of ${height}.`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers)=>{
	return console.log(numbers);
});

const reduceNumber = (setNumbers, setNumbers2) => setNumbers + setNumbers2
console.log(numbers.reduce(reduceNumber));

class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Chuchu', 1, 'Aspin')
console.log(myDog);